import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  heure = "";



  constructor() {
    setInterval(() => {
      // Permet de faire defiler l'affichage de l'horloge avec un interval de 1 seconde (1000 ms)
      const d = new Date();
      this.heure = d.toLocaleTimeString();
    }, 1000);
  }

  ngOnInit(): void {
  }

}
