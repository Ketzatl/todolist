import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {
  todoOne: string = "Projet 1";
  todoTwo: string = "Projet 2";
  todoThree: string = "Projet 3";
  todoFour: string = "Projet 4";

  todos: string[] = ["Projet t1", "Projet t2", "Projet t3", "Projet t4", "Projet t5",
    "Projet t6", "Projet t7", "Projet t8"];

  constructor() { }

  ngOnInit(): void {
  }

}
